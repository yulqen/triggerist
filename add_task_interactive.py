import os
import sys
from todoist_api_python.api import TodoistAPI

def list_projects(api):
    projects = api.get_projects()
    print("Available Projects:")
    for i, project in enumerate(projects):
        print(f"{i+1}. {project.name}")
    return projects

def list_labels(api):
    labels = api.get_labels()
    print("Available Labels:")
    for i, label in enumerate(labels):
        print(f"{i+1}. {label.name}")
    return labels

def add_task_to_project(api, task_name, project_id, labels, due_date=None):
    task_options = {"content": task_name, "project_id": project_id}

    if due_date:
        task_options["due_string"] = due_date

    if labels:
        label_names = [api.get_label(label_id).name for label_id in labels]
        task_options["labels"] = label_names

    item = api.add_task(**task_options)

    due_date_msg = f" with due date '{due_date}'" if due_date else ""
    label_msg = f" and labels '{label_names}'" if labels else ""
    print(f"Task '{task_name}' added to project '{api.get_project(project_id)}'{due_date_msg}{label_msg}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python add_task.py <task_name>")
        sys.exit(1)

    task_name = sys.argv[1]
    api_key = os.getenv("TODOIST_API_KEY")

    if not api_key:
        print("Please set the TODOIST_API_KEY environment variable.")
        sys.exit(1)

    api = TodoistAPI(api_key)
    projects = list_projects(api)

    try:
        selected_project_index = int(input("Enter the number of the project to add the task to: ")) - 1
        if 0 <= selected_project_index < len(projects):
            due_date = input("Enter the due date in natural language (e.g., 'tomorrow') or leave empty for no due date: ").strip()
            labels = list_labels(api)
            selected_labels = []

            while True:
                selected_label_index = input("Enter the number of the label to add or leave empty to continue: ").strip()
                if not selected_label_index:
                    break
                
                try:
                    selected_label_index = int(selected_label_index) - 1
                    if 0 <= selected_label_index < len(labels):
                        selected_labels.append(labels[selected_label_index].id)
                    else:
                        print("Invalid label number.")
                except ValueError:
                    print("Invalid input. Please enter a valid number.")

            add_task_to_project(api, task_name, projects[selected_project_index].id, selected_labels, due_date if due_date else None)
        else:
            print("Invalid project number. Exiting.")
    except ValueError:
        print("Invalid input. Please enter a valid number.")

